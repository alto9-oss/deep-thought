const HealthController = require('./controllers/health.controller');

exports.routesConfig = function (app) {
    app.get('/health/', [
        HealthController.checkHealth
    ]);
}