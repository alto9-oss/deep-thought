/*
Webhook Payload

{
    "ticker": "{{ticker}}",
    "action": "{{strategy.order.action}}",
    "price": "{{close}}"
}
*/
const AlpacaController = require('../../controllers/alpaca.controller')

const alpaca = new AlpacaController()

exports.handleTrade = async (req, res) => {
    const trade = req.body
    const key = req.params.key
    const type = req.params.type

    //TODO: validate the key

    let account = await alpaca.getAccount()

    if(account.trading_blocked || account.account_blocked){
        res.status(401).send('Alpaca has blocked your account from trading. Might want to give them a call.')
    }

    if(trade.action == "buy"){
        //determine how many contracts to purchase to equal 5% of portfolio value
        let contracts = Math.floor((account.portfolio_value * .05) / trade.price)
        //we have to be able to afford at least one share
        if(contracts == 0)
            res.status(401).send('Stock price is too high')
        //only allow buy if we are not overextended
        if(account.cash < account.long_market_value){
            res.status(401).send('Cash floor limit reached')
        }
        //determine the order details
        let limitThreshold = 0.0
        let stopThreshold = 0.0
        switch(type){
            case 'day':
                limitThreshold = .006
                stopThreshold = .003
            break;
            case 'swing':
                limitThreshold = .02
                stopThreshold = .01
            break;
        }
        let price = parseFloat(trade.price).toFixed(2)
        let limitPrice = (parseFloat(price) + (price * limitThreshold)).toFixed(2)
        let stopPrice = (parseFloat(price) - (price * stopThreshold)).toFixed(2)
        //execute and track the order
        let tradeOrder = alpaca.executeBracketOrder(trade.ticker, contracts, limitPrice, stopPrice)
        
        res.status(200).send(tradeOrder)

    } else if(trade.action == "sell"){
        alpaca.closePosition(trade.ticker)
    }
}
