const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const HealthRouter = require('./health/routes.config')
const TradingViewRouter = require('./tradingview/routes.config')

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Credentials', 'true')
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE')
    res.header('Access-Control-Expose-Headers', 'Content-Length')
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range')
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200)
    } else {
        return next()
    }
});

app.use(bodyParser.json())
app.use(express.urlencoded()) // to support URL-encoded bodies

HealthRouter.routesConfig(app)
TradingViewRouter.routesConfig(app)

app.listen(8080, function () {
    console.log('app listening at port %s', 8080)

    if(process.env.ALPACA_PAPER){
        console.log("_________ PAPER TRADING MODE _________")
    } else {
        console.log("********* LIVE TRADING MODE ***********")
    }
});